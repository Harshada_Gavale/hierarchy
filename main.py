import constants
from gevent import monkey
import csv

monkey.patch_all()
import copy

from flask import Flask, render_template, url_for, send_file, jsonify, request, session, send_from_directory, redirect
from flask_socketio import SocketIO, emit, join_room, leave_room
from flask_session import Session
import time
import datetime
from werkzeug.middleware.proxy_fix import ProxyFix
import json

import uuid

app = Flask(__name__)
app.config['SECRET_KEY'] = 'top-secret!'
app.config['SESSION_TYPE'] = 'filesystem'
Session(app)
# socketio = SocketIO(app, manage_session=False)
# app.wsgi_app = ProxyFix(app.wsgi_app, x_for=1, x_proto=1)
socketio = SocketIO(app, manage_session=False, cors_allowed_origins="*")

import database_operations
import otp_operations
import threading

import new_database_operations

import read_planning_sheet

planning_sheet_save_path = "../Planning Sheets Upload/"
SPR_path = "SPR"
import os

@socketio.on("connected")
def connected():
    print("connected")


@socketio.on("Log In")
def Log_In():
    print("Log In")


@socketio.on("check_user")
def check_user(data):
    room = data["phone"]
    join_room(room)
    session["user"] = data["phone"]
    session['o_id'] = data['o_id']
    session['login'] = False
    print(session)
    Val = database_operations.check_user(data)
    if Val == 0:
        socketio.emit("check_user_status", "User exists", room=room)
    else:
        socketio.emit("check_user_status", "User does not exist", room=room)


@socketio.on("Sign_Up_Request_1")
def sign_up_request_1(data):
    room = data["phone"]
    join_room(room)
    session["user"] = data["phone"]
    session['o_id'] = data['o_id']
    session['login'] = False
    print(session)
    Val = database_operations.signup_user(data)
    if Val == True:
        socketio.emit("sign_up_status", "Verify your number using the OTP we just shared with you.", room=room)
    else:
        socketio.emit("sign_up_status", "This number already exists in organisation.", room=room)


@socketio.on("Sign_Up_Request")
def sign_up_request(data):
    room = session['user']
    join_room(room)
    data["phone"] = session["user"]
    session['name'] = data["name"]
    # data['o_id'] = session['o_id']
    session['login'] = False
    print(session)
    Val = database_operations.signup_user_data(data)
    if Val == True:
        socketio.emit("sign_up_status", "Redirecting ...", room=room)
    else:
        socketio.emit("sign_up_status", "This name and number already exists in organisation.", room=room)


@socketio.on("verify_otp_data")
def verify_otp_data(data):
    print("In verify otp data")
    if 'user' in session:
        phone = session['user']
        room = session['user']
        join_room(room)
        otp = database_operations.return_otp(phone)
        print(str(otp['otp']))
        print(data)
        if str(otp['otp']) == str(data):
            database_operations.update_otp_verification(phone)
            socketio.emit("otp_status", "Redirecting ...", room=room)
            print("OTP successful")
        else:
            print("OTP not succesful but user found")
            socketio.emit("otp_status", "Verification not successful.", room=room)
        leave_room(room)
    else:
        room2 = request.remote_addr
        join_room(room2)
        print("OTP not succesful")
        socketio.emit("otp_status", "Verification not successful.", room=room2)
        leave_room(room2)


@socketio.on("verify_otp_data_partial")
def verify_otp_data_partial(data):
    print("In verify otp data")
    if 'user' in session:
        phone = session['user']
        room = session['user']
        join_room(room)
        otp = database_operations.return_otp(phone)
        print(str(otp['otp']))
        print(data)
        if str(otp['otp']) == str(data):
            database_operations.update_otp_verification_partial(phone)
            socketio.emit("otp_status", "Redirecting ...", room=room)
            print("OTP successful")
        else:
            print("OTP not succesful but user found")
            socketio.emit("otp_status", "Verification not successful.", room=room)
        leave_room(room)
    else:
        room2 = request.remote_addr
        join_room(room2)
        print("OTP not succesful")
        socketio.emit("otp_status", "Verification not successful.", room=room2)
        leave_room(room2)


@socketio.on("resend_otp")
def resend_otp():
    if 'user' in session:
        phone = session['user']
        print("Found User")
        Val = database_operations.update_otp(phone)
        print("Update requested")
    else:
        room2 = request.remote_addr
        join_room()
        socketio.emit("otp_status", "Verification not successful.", room=room2)
        leave_room(room2)


@socketio.on("login_cred")
def login_cred(data):
    room = data['phone']
    join_room(room)
    print(data)
    Val = database_operations.verify_credentials(data)
    if Val == 2:
        session["user"] = data["phone"]
        # session['role'] = data["role"]
        data1 = database_operations.return_credentials(data['phone'])
        session['role'] = data1['role']
        session['name'] = data1['name']
        session['o_id'] = data1['o_id']
        session["login"] = True
        socketio.emit("login_status", "2", room=room)
        leave_room(room)
    if Val == 1:
        session["user"] = data["phone"]
        # session['role'] = data["role"]
        data1 = database_operations.return_credentials(data['phone'])
        session['role'] = data1['role']
        session['name'] = data1['name']
        session['o_id'] = data1['o_id']
        session["login"] = False
        socketio.emit("login_status", "1", room=room)
    if Val == 0:
        socketio.emit("login_status", "0", room=room)


@socketio.on("login_cred_check")
def login_cred_check(data):
    room = data['phone']
    join_room(room)
    print(data)
    Val = database_operations.verify_credentials(data)
    if Val == 2:
        session["user"] = data["phone"]
        # session['role'] = data["role"]
        data1 = database_operations.return_credentials(data['phone'])
        session['role'] = data1['role']
        session['name'] = data1['name']
        session['o_id'] = data1['o_id']
        session["login"] = True
        socketio.emit("login_status_check", "2", room=room)
        leave_room(room)
    if Val == 1:
        session["user"] = data["phone"]
        # session['role'] = data["role"]
        data1 = database_operations.return_credentials(data['phone'])
        session['role'] = data1['role']
        session['name'] = data1['name']
        session['o_id'] = data1['o_id']
        session["login"] = False
        socketio.emit("login_status_check", "1", room=room)
    if Val == 0:
        socketio.emit("login_status_check", "0", room=room)


@app.route('/autologin', methods=['GET'])
def autologin():
    data = {}
    data['phone'] = request.args.get('phone')
    data['password'] = request.args.get('password')

    print("----------------------------")
    print(data)
    print("----------------------------")
    # room = data["phone"]
    # join_room(room)
    # print(data)
    Val = database_operations.verify_credentials(data)
    if Val == 2:
        session["user"] = data["phone"]
        # session['role'] = data["role"]
        data1 = database_operations.return_credentials(data['phone'])
        session['role'] = data1['role']
        session['name'] = data1['name']
        session['o_id'] = data1['o_id']
        session["login"] = True
        # socketio.emit("login_status","2",room=room)

        # -------------------------------------------------------------------------

        if 'user' in session and 'login' in session:
            if session['login']:
                return redirect(url_for('epass'))
                '''color="info"
                phone = session['user']
                epass = database_operations.get_epass(phone)
                #print(epass)
                timestamp = ""
                button_text = ""
                if "timestamp" in  epass :
                    timestamp ="Last record on : "+  epass['timestamp'].strftime("%d/%m/%Y %H:%M:%S")
                    if (datetime.datetime.now() + datetime.timedelta(minutes=330) - epass['timestamp'] ) > datetime.timedelta(1):
                        #print("1 day elapsed")
                        epass['epass_status']="No Application found in last 24 hours."
                        epass['qrcode_link'] = '/epass/fillup.jpg'
                        #print(timestamp)
                        color = "warning"
                        button_text = "Apply for E-Pass"
                    if epass['epass_status'] == 'Accepted' :
                        color = "success"
                        button_text = "Re-apply for E-Pass"
                    if epass['epass_status'] == 'Rejected':
                        color = "danger"
                        button_text = "Retry for E-Pass"
                    #print(color)
                else :
                    epass['epass_status'] = "You do not have any E-Pass Applications. Please answer questions in Self-Declaration section to get an E-Pass"
                    epass['qrcode_link'] = '/epass/fillup.jpg'
                    button_text = "Go to Self-Declaration "
					
			
				
				
                return render_template("epass.html",status=epass['epass_status'],
                                       last_app=timestamp,
                                       src=epass["qrcode_link"],
                                       color=color,
                                       active_page ='epass',
                                       role=session['role'],
                                       name=session['name'],
                                       button_text = button_text
                                       )'''


        else:
            return redirect(url_for('login'))

        # ----------------------------------------------------------------------------------------------------------------------

        # leave_room(room)
    if Val == 1:
        session["user"] = data["phone"]
        # session['role'] = data["role"]
        data1 = database_operations.return_credentials(data['phone'])
        session['role'] = data1['role']
        session['name'] = data1['name']
        session['o_id'] = data1['o_id']
        session["login"] = False
        # render_template('sign.html')
        return redirect(url_for('login'))
        ###return render_template("signup.html")
        # socketio.emit("login_status", "1",room=room)
    if Val == 0:
        # socketio.emit("login_status", "0",room=room)
        return redirect(url_for('login'))


@socketio.on("forgot_generate_otp")
def forgot_generate_otp(phone):
    room = phone
    join_room(phone)
    Val = database_operations.update_otp(phone)
    print("Update requested")
    leave_room(phone)
    # Send otp


@socketio.on("forgot_verify_otp")
def forgot_verify_otp(data):
    room = data["phone"]
    join_room(room)
    otp = database_operations.return_otp(data['phone'])
    if str(otp['otp']) == str(data['otp']):
        socketio.emit("otp_status", "Redirecting ...", room=room)
        print("OTP successful")
        session['user'] = data['phone']
        leave_room(room)
    else:
        print("OTP not succesful but user found")
        socketio.emit("otp_status", "Verification not successful.", room=room)


@socketio.on("reset_password")
def forgot_verify_otp(password):
    if 'user' in session:
        room = session['user']
        join_room(room)
        data = {}
        data['phone'] = session['user']
        data['password'] = password
        print("Found User")
        Val = database_operations.reset_password(data)
        if Val == True:
            socketio.emit("update_pass_status", "True", room=room)
            leave_room(room)
        else:
            socketio.emit("update_pass_status", "False", room=room)


@socketio.on("logout")
def logout():
    if 'user' in session:
        session.pop('user')
        session.pop("role")
        session.pop("name")
        session.pop('o_id')
        session.pop('login')
        print("Logged Out")
    else:
        print("User not found !")


@socketio.on("declaration_data")
def declaration(data):
    print(data)
    if 'user' in session:
        phone = session['user']
        room = phone
        join_room(room)
        Val = database_operations.update_declaration(phone, data, session)
        time.sleep(1)
        socketio.emit("declaration_submitted", room=room)
        # leave_room(room)


@app.route('/template')
def template():
    return render_template("template.html")


# ===================================================

@app.route('/visitor')
def visitor():
    return render_template("visitor.html")


@app.route('/visitor_epass')
def visitor_epass():
    return render_template("visitor_epass.html")


@socketio.on("visitor_Request")
def visitor_Request(dt):
    room = request.sid
    join_room(room)
    print("Data Requested")
    print(dt)
    status, link = database_operations.visitor_request(dt)
    socketio.emit("visitor_application", {"status": status, "link": link}, room=room)


@app.route('/temperature')
def temperature():
    if 'user' in session and 'login' in session:
        if session['login']:
            user_temp = database_operations.retrive_user_temp(session['user'])
            user_flag = True
            if len(user_temp) == 1:
                user_flag = False
            return render_template("temperature.html",
                                   active_page='temperature',
                                   role=session['role'],
                                   name=session['name'],
                                   user_temp=user_temp,
                                   user_flag=user_flag
                                   )
    else:
        return redirect(url_for('login'))


# ===================================================


@app.route('/epass/<path:filename>')
def download_file(filename):
    print('correct loop')
    return send_from_directory('code', filename, as_attachment=True)


@app.route('/login')
def login():
    if 'user' in session:
        phone = session['user']
        credentials = database_operations.return_credentials(phone)
        return render_template("signin.html", phone=phone, password=credentials['password'],
                               role=credentials['role'], o_id=credentials['o_id'])
    else:
        return render_template("signin.html")


@app.route('/createaccount')
def createaccount():
    return render_template("createaccount.html")


@app.route('/signup')
def signup():
    questions = database_operations.get_signup_questions()
    return render_template("signup.html")


@app.route('/signup1')
def signup1():
    return render_template("signup1.html")


@app.route('/signup2')
def signup2():
    return render_template("signup2.html")


@app.route('/verifyotp')
def verifyotp():
    if 'user' in session:
        print(session['user'])
        return render_template("verifyotp.html", user=session["user"])
    else:
        return render_template("signup.html")


@app.route('/forgotpassword')
def forgotpassword():
    return render_template("forgotpassword.html")


@app.route('/newpassword')
def newpassword():
    if 'user' in session:
        return render_template("newpassword.html")
    else:
        return render_template("signup.html")


@app.route('/epass')
def epass():
    if 'user' in session and 'login' in session:
        if session['login']:
            color = "info"
            phone = session['user']
            epass = database_operations.get_epass(phone)
            # print(epass)
            timestamp = ""
            button_text = ""
            new_timestamp = ""
            if "timestamp" in epass:
                timestamp = "Last record on : " + epass['timestamp'].strftime("%d/%m/%Y %H:%M:%S")
                new_timestamp = epass['timestamp'].strftime("%d %B, %Y")
                print("---------------------------", new_timestamp)
                # if (datetime.datetime.now() + datetime.timedelta(minutes=330).date() != epass['timestamp'].date() ) > datetime.timedelta(minutes=5):
                if (datetime.datetime.now() + datetime.timedelta(minutes=330)).date() != epass['timestamp'].date():
                    # print("1 day elapsed")
                    epass['epass_status'] = "No Application found in last 24 hours."
                    epass['qrcode_link'] = '/epass/fillup.jpg'
                    # print(timestamp)
                    color = "warning"
                    button_text = "Apply for E-Pass"
                if epass['epass_status'] == 'Accepted':
                    color = "success"
                    button_text = "Re-apply for E-Pass"
                if epass['epass_status'] == 'Rejected':
                    color = "danger"
                    button_text = "Retry for E-Pass"
                # print(color)
            else:
                epass[
                    'epass_status'] = "You do not have any E-Pass Applications. Please answer questions in Self-Declaration section to get an E-Pass"
                epass['qrcode_link'] = '/epass/fillup.jpg'
                button_text = "Go to Self-Declaration "

            scan_time = "--"
            temperature = "-"
            scan_unit = "-"
            print(session["o_id"])
            val, scan_settings_data = database_operations.get_oid_settings(session["o_id"])

            if val == 1:
                scan_unit = scan_settings_data["scan_unit"]
                scan_threshold = float(scan_settings_data["threshold_scan"])
                val, temperature_data = database_operations.get_latest_temmperature(phone)
                if val == 1:
                    temperature = temperature_data["temperature"]
                    scan_time = temperature_data["timestamp"].strftime("%d/%m/%Y :: %H:%M:%S")
                    print(temperature)
                    if float(temperature_data["temperature"]) > scan_threshold:
                        if (datetime.datetime.now() + datetime.timedelta(minutes=330)).date() == temperature_data[
                            "timestamp"].date():
                            epass["epass_status"] = "high_temperature"

            print(epass['qrcode_link'])

            database_operations.get_freeze_status(phone)

            return render_template("epass.html", status=epass['epass_status'],
                                   last_app=new_timestamp,
                                   scan_time=scan_time,
                                   src=epass["qrcode_link"],
                                   color=color,
                                   active_page='epass',
                                   role=session['role'],
                                   name=session['name'],
                                   button_text=button_text,
                                   temperature=temperature,
                                   scan_unit=scan_unit,
                                   account_freeze=database_operations.get_freeze_status(phone)
                                   )
    else:
        return redirect(url_for('login'))


@app.route('/declaration')
def declaration():
    if 'user' in session and 'login' in session:
        if session['login']:
            return render_template("declaration.html",
                                   active_page='declaration',
                                   role=session['role'],
                                   name=session['name'],
                                   )
    else:
        return redirect(url_for('login'))


@app.route('/distancing')
def distancing():
    if 'user' in session and 'login' in session:
        if session['login']:
            return render_template("distancing.html",
                                   active_page='distancing',
                                   role=session['role'],
                                   name=session['name'],
                                   )
    else:
        return redirect(url_for('login'))


@app.route('/security')
def secrutiy():
    if 'user' in session and 'login' in session:
        if session['login']:
            return render_template("security.html",
                                   active_page='security',
                                   role=session['role'],
                                   name=session['name'],
                                   )
    else:
        return redirect(url_for('login'))


@app.route('/manualsecurity')
def manualsecrutiy():
    if 'user' in session and 'login' in session:
        if session['login']:
            return render_template("manual_security.html",
                                   active_page='security',
                                   role=session['role'],
                                   name=session['name'],
                                   )
    else:
        return redirect(url_for('login'))


@app.route('/contact')
def contact():
    if 'user' in session and 'login' in session:
        if session['login']:
            return render_template("contact.html",
                                   active_page='contact',
                                   role=session['role'],
                                   name=session['name'],
                                   )
    else:
        return redirect(url_for('login'))


@app.route('/admin')
def admin():
    if 'user' in session and 'login' in session:
        if session['login']:
            return render_template("admin.html",
                                   active_page='admin',
                                   role=session['role'],
                                   name=session['name'],
                                   )
    else:
        return redirect(url_for('login'))


# --------------------------------------trace ----------------------------------
@app.route('/trace')
def trace():
    if 'user' in session and 'login' in session:
        if session['login']:
            return render_template("trace.html",
                                   active_page='trace',
                                   role=session['role'],
                                   name=session['name'],
                                   )
    else:
        return redirect(url_for('login'))


@socketio.on("trace")
def trace_data(input):
    if 'user' in session and 'login' in session:
        if session['login']:
            room = session['user']
            join_room(room)
            val, data = database_operations.trace_data(input, session['o_id'])
            if val == True:
                socketio.emit("trace_data", data, room=room)
            else:
                socketio.emit("trace_data_status", "Employee not found", room=room)


# -----------------------------attendance--------------------------------------------


@app.route('/attendance')
def attendance():
    if 'user' in session and 'login' in session:
        if session['login']:
            return render_template("attendance.html",
                                   active_page='attendance',
                                   role=session['role'],
                                   name=session['name'],
                                   )
    else:
        return redirect(url_for('login'))


@socketio.on("attendance_data")
def attendance_data(flag):
    if flag == "null":
        if 'user' in session:
            room = session['user']
            join_room(room)

            current_time = datetime.datetime.now()  # + datetime.timedelta(minutes=330)
            to_date = current_time.strftime("%Y-%m-%d")
            one_day_prior = current_time - datetime.timedelta(hours=24)
            from_date = one_day_prior.strftime("%Y-%m-%d")
            to_send = {"from_date": from_date, "to_date": to_date}
            socketio.emit("set_date", to_send, room=room)
            print(to_send)
            data = database_operations.attendance_data(session['o_id'], to_send)
            socketio.emit("reciv_attendance", data, room=room)
    else:
        if 'user' in session:
            room = session['user']
            join_room(room)
            data = database_operations.attendance_data(session['o_id'], flag)
            socketio.emit("reciv_attendance", data, room=room)


@socketio.on("modal_attendance")
def modal_attendance(data):
    if 'user' in session:
        room = session['user']
        join_room(room)
        data = database_operations.attendance_modal(data, session['o_id'])

        socketio.emit("modal_data", data, room=room)


@socketio.on("date_wise_attendance")
def date_wise_attendance(date):
    if 'user' in session:
        room = session['user']
        join_room(room)

        format_str = '%d/%m/%Y'  # The format
        start_time = datetime.datetime.strptime(date, format_str)


        print(start_time.date())

        '''socketio.emit("set_date", date, room=room)

        data = database_operations.date_wise_attendance(date,session['o_id'])
        socketio.emit("reciv_attendance",data,room=room)'''


# ------------------------------visitors-----------------------------------

@app.route('/visitors')
def visitors():
    if 'user' in session and 'login' in session:
        if session['login']:
            return render_template("visitors.html",
                                   active_page='visitors',
                                   role=session['role'],
                                   name=session['name'],
                                   )
    else:
        return redirect(url_for('login'))


@socketio.on("visitor_data")
def visitor_data():
    if 'user' in session:
        room = session['user']
        join_room(room)

        current_time = datetime.datetime.now() + datetime.timedelta(minutes=330)
        date = current_time.strftime("%d/%m/%Y")
        socketio.emit("set_date", date, room=room)

        data = database_operations.visitor_data(session['o_id'])
        socketio.emit("reciv_visitor", data, room=room)


@socketio.on("modal_visitor")
def modal_visitor(data):
    if 'user' in session:
        room = session['user']
        join_room(room)
        data = database_operations.visitor_modal(data, session['o_id'])
        print(data)
        socketio.emit("modal_data_visitor", data, room=room)


@socketio.on("date_wise_visitor")
def date_wise_visitor(date):
    if 'user' in session:
        room = session['user']
        join_room(room)
        socketio.emit("set_date", date, room=room)

        data = database_operations.date_wise_visitor(date, session['o_id'])
        socketio.emit("reciv_visitor", data, room=room)


# --------------------------------------------Applications-----------------------

@app.route('/applications')
def application():
    if 'user' in session and 'login' in session:
        if session['login']:
            return render_template("application.html",
                                   active_page='applications',
                                   role=session['role'],
                                   name=session['name'],
                                   )
    else:
        return redirect(url_for('login'))


@socketio.on("application_data")
def application_data():
    if 'user' in session:
        room = session['user']
        join_room(room)

        data = database_operations.application_data(session['o_id'])
        socketio.emit("application", data, room=room)


@socketio.on("change_status")
def change_status(arr):
    if 'user' in session:
        room = session["user"]
        join_room(room)
        data = database_operations.change_status(arr, session['o_id'])
        # socketio.emit("application",data,room=room)


@socketio.on("question")
def cquestion(question):
    if 'user' in session:
        room = session["user"]
        join_room(room)
        data = database_operations.log_question(question, session)
        # socketio.emit("application",data,room=room)


@socketio.on("modal_application")
def modal_application(x):
    if 'user' in session:
        room = session["user"]
        join_room(room)
        data = database_operations.modal_application(x, session['o_id'])
        socketio.emit("modal_data_application", data, room=room)


@socketio.on("updated_role")
def updated_role(arr):
    if 'user' in session:
        room = session["user"]
        data = database_operations.update_role(arr, session['o_id'])
        return "ok"


@socketio.on("updated_employee_status")
def updated_employee_status(arr):
    print(arr)
    if 'user' in session:
        room = session["user"]
        data = database_operations.update_employee_type(arr, session['o_id'])
        return "ok"


# ==========================================================================

@socketio.on("manual_security")
def manual_security(data):
    if 'user' in session:
        room = session["user"]
        join_room(room)
        val, temp_ok = database_operations.manual_temp_entry(data, session)
        if val == False:
            socketio.emit("manual_scurity_status", "not_a_person", room=room)
        else:
            if temp_ok == False:
                socketio.emit("manual_scurity_status", "temp_high", room=room)
            if temp_ok == True:
                socketio.emit("manual_scurity_status", "temp_ok", room=room)
            # print(data)


@socketio.on("scanned_security")
def scanned_security(data):
    if 'user' in session:
        room = session["user"]
        join_room(room)
        val, temp_ok = database_operations.scanned_security(data, session)
        if val == False:
            socketio.emit("manual_scurity_status", "not_a_person", room=room)
            socketio.emit("manual_scurity_status", "not_a_person")
        else:
            if temp_ok == False:
                socketio.emit("manual_scurity_status", "temp_high", room=room)
                socketio.emit("manual_scurity_status", "temp_high", room=room)
            if temp_ok == True:
                socketio.emit("manual_scurity_status", "temp_ok", room=room)
        leave_room(room)

        if val == False:
            socketio.emit("broadcast_scurity_status", "not_a_person")
            socketio.emit("broadcast_scurity_status", "not_a_person")
        else:
            if temp_ok == False:
                socketio.emit("broadcast_scurity_status", "temp_high")
                socketio.emit("broadcast_scurity_status", "temp_high")
            if temp_ok == True:
                socketio.emit("broadcast_scurity_status", "temp_ok")


# Additional Post Event for Android Scanning Activity
@app.route('/scan_android', methods=['POST', 'GET'])
def scanned_android():
    data = {}
    session = {}
    session['name'] = "Android App"
    if request.method == 'POST':
        print(request.json)
        data = request.json
        '''if data == None
            if 'uid' and  'temperature'
            data['uid'] = request.form['uid']
            data['temperature'] = request.form['temperature']
            data['mask'] = request.form['mask']'''
        print("------Scan Recieved-----")
        if "uid" in data:
            if len(data['uid']) > 32:
                data['uid'] = (data['uid'][0:32]).lower()
                print(data["uid"])
                print("------------------------")
                val, temp_ok = database_operations.scanned_security(data, session)
                if val == False:
                    return jsonify({"response": "not_a_person"})
                else:
                    if temp_ok == False:
                        return jsonify({"response": "temp_high"})

                    if temp_ok == True:
                        return jsonify({"response": "temp_ok"})
            else:
                return jsonify({"response": "not_a_person"})
    # return "OK"

    # return redirect(url_for('success',name = user))


# Additional Post Event for Android Scanning Activity
@app.route('/scan_hardware', methods=['POST', 'GET'])
def scan_hardware():
    data = {}
    session = {}
    session['name'] = "C4i4 Hardware"
    if request.method == 'GET':
        record = request.args.get('record')
        print("------------------")
        print("GET EVENT RECEIVED")
        print(record)
        if len(record) >= 50:
            uid = record[0:32]
            temperature = record[32:36]
            date = record[36:50]

            print(uid)
            print(temperature)
            print(date)

            format_str = '%d%m%Y%H%M%S'  # The format
            timestamp = datetime.datetime.strptime(date, format_str)
            # print(datetime_obj.)

            data['uid'] = uid
            data['temperature'] = float(temperature[0:2] + "." + temperature[2:4])
            data['mask'] = "yes"
            print("------------------------")
            val, temp_ok = database_operations.scanned_hardware(data, session, timestamp)
            print("------------------")
            if val == False:
                # return jsonify({"response": "not_a_person"})
                return "0"
            else:
                if temp_ok == False:
                    # return jsonify({"response": "temp_high"})
                    return "1"
                if temp_ok == True:
                    # return jsonify({"response": "temp_ok"})
                    return "1"
        else:
            # return jsonify({"response": "INVALID QUERY"})
            return "0"


# ===========================================================================

@app.route('/masterlogin')
def masterlogin():
    return render_template("master-signin.html")


@socketio.on("master_login_credentials")
def master_login_credentials(data):
    room = request.sid
    join_room(room)
    print("Master Login Credentials received")
    if data["phone"] == "Admin" and data["password"] == "Anzen2020":
        socketio.emit("master_credentials_validation", "1", room=room)
        session["master_user"] = True
    else:
        socketio.emit("master_credentials_validation", "0", room=room)
        session["master_user"] = False


# ----------------- MAster Application -------------
@app.route('/master-applications')
def masterapplications():
    if 'master_user' in session:
        if session['master_user']:
            return render_template("master-application.html",
                                   active_page='master-applications',
                                   )
    else:
        return redirect(url_for('masterlogin'))


@socketio.on("master_application_data")
def master_application_data():
    if 'master_user' in session:
        room = request.sid
        join_room(room)

        data = database_operations.master_application_data()
        socketio.emit("master_application", data, room=room)


@socketio.on("master_change_status")
def master_change_status(arr):
    if 'master_user' in session:
        room = request.sid
        join_room(room)
        data = database_operations.master_change_status(arr)
        socketio.emit("master_application", data, room=room)


@socketio.on("master_modal_application")
def master_modal_application(x):
    if 'master_user' in session:
        room = request.sid
        join_room(room)
        data = database_operations.master_modal_application(x)
        socketio.emit("master_modal_data_application", data, room=room)


@socketio.on("master_updated_role")
def master_updated_role(arr):
    if 'master_user' in session:
        room = request.sid
        print("Requested Role Update")
        data = database_operations.master_update_role(arr)
        return "ok"


@socketio.on("master_updated_employee_status")
def updated_employee_status(arr):
    print(arr)
    if 'master_user' in session:
        room = request.sid
        print("Reqested Employee Status Updated ")
        data = database_operations.master_update_employee_type(arr)
        return "ok"


@socketio.on("master_logout")
def master_logout():
    if 'master_user' in session:
        session.pop('master_user')
        print("Master Logged Out")
    else:
        print("User not found !")


@socketio.on("save_question_changes")
def save_question_changes(data):
    if 'master_user' in session:
        room = request.sid
        data, val = database_operations.save_questions(data)
        socketio.emit("master_console", "Saved !", room=room)
    else:
        print("User not found !")


@socketio.on("customer_onboarding")
def customer_onboarding(data):
    if 'master_user' in session:
        room = request.sid
        data, val = database_operations.on_board_customer(data)
        if data == 0:
            socketio.emit("master_console", "Customer with Same Organisation ID exists !", room=room)
        else:
            socketio.emit("master_console", "Congratulations ! New Customer On-boarded .", room=room)
    else:
        print("User not found !")


@app.route('/master-onboarding')
def master_onboarding():
    if 'master_user' in session:
        if session['master_user']:
            return render_template("master-onboarding.html",
                                   active_page='master-onboarding',
                                   )
    else:
        return redirect(url_for('masterlogin'))


@app.route('/master-questions')
def master_questions():
    if 'master_user' in session:
        if session['master_user']:
            return render_template("master-questions.html",
                                   active_page='master-questions',
                                   )
    else:
        return redirect(url_for('masterlogin'))


@socketio.on("get_master_questions")
def get_master_questions():
    if 'master_user' in session:
        room = request.sid
        join_room(room)
        data = database_operations.get_questions()
        print("--------------------------")
        print(data)
        print("--------------------------")

        socketio.emit("sent_master_questions", data, room=room)


@app.route('/all_questions')
def all_questions():
    if 'user' in session and 'login' in session:
        if session['login']:
            return render_template(".html",
                                   active_page='settings',
                                   role=session['role'],
                                   name=session['name'],
                                   )
    else:
        return redirect(url_for('login'))


# -----------------------------------------------------
@app.route('/settings')
def settings():
    if 'user' in session and 'login' in session:
        if session['login']:
            return render_template("settings.html",
                                   active_page='settings',
                                   role=session['role'],
                                   name=session['name'],
                                   )
    else:
        return redirect(url_for('login'))


@socketio.on("settings_info")
def get_settings_info():
    if 'user' in session:
        room = session["user"]
        room = request.sid
        join_room(room)
        val, data = database_operations.get_settings(session['o_id'])
        print(data)
        socketio.emit("settings_info_return", data)
        leave_room(room)
        return "ok"


@socketio.on("change_settings")
def change_settings(data):
    if 'user' in session:
        room = session["user"]
        room = request.sid
        join_room(room)
        database_operations.change_settings(session['o_id'], data)
        socketio.emit("settings_saved")
        leave_room(room)
        return "ok"


# -----------------manual_user_entry------------------
@socketio.on("manual_user")
def manual_user(data):
    print(data)
    if 'user' in session:
        o_id = session['o_id']
        Val = database_operations.manual_user(data, o_id)
        socketio.emit("manual_user_response", str(Val))


@socketio.on("qrcode_request")
def qrcode_request():
    if 'user' in session:
        o_id = session['o_id']
        Val = database_operations.qrcode_list_request(o_id)
        socketio.emit("qrcode_list_received", Val)


@app.route('/qrcode')
def qrcode():
    return render_template("qrcode_list.html")


# -----------------------------------
# UPDATE
@app.route('/update')
def update():
    return render_template("update.html")


@app.route("/update_app", methods=['GET'])
def update_app():
    return render_template('update_app.html', value="APK Download")


@app.route('/return-files/<filename>')
def return_files_tut(filename):
    filepath = "Anzen_update.apk"
    return send_file(filepath, as_attachment=True, attachment_filename='Anzen_update.apk')


@socketio.on("get_daily_declaration_questions")
def get_daily_declaration_questions():
    if 'user' in session:
        o_id = session['o_id']
        room = request.sid
        join_room(room)
        data = database_operations.get_daily_questions(o_id)
        print("--------------------------")
        print(data)
        print("--------------------------")
        socketio.emit("sent_daily_declaration_questions", data, room=room)


@socketio.on("get_signup_declaration_questions")
def get_signup_declaration_questions():
    if 'user' in session:
        o_id = session['o_id']
        room = request.sid
        join_room(room)
        data = database_operations.get_signup_questions(o_id)
        print("--------------------------")
        print(data)
        print("--------------------------")
        socketio.emit("sent_signup_declaration_questions", data, room=room)


@socketio.on("declaration_data_new")
def declaration_new(data):
    print("--------------------------")
    print(data)
    print("--------------------------")

    if 'user' in session:
        phone = session['user']
        room = phone
        join_room(room)
        Val = database_operations.update_declaration_new(phone, data, session)
        time.sleep(1)
        socketio.emit("declaration_submitted", room=room)
        # leave_room(room)


@socketio.on("organisation_settings_change")
def organisation_settings_change(data):
    if 'master_user' in session:
        room = request.sid
        data, val = database_operations.organisation_settings_change(data, session['o_id'])
        if data == 0:
            socketio.emit("master_console", "Settings not saved.", room=room)
        else:
            socketio.emit("master_console", "Settings saved successfully !", room=room)
    else:
        print("User not found !")


@socketio.on("get_settings_oid")
def get_settings_oid():
    if 'user' in session:
        o_id = session['o_id']
        room = request.sid
        data = database_operations.get_settings_oid(o_id)
        if data == []:
            pass
        else:
            socketio.emit("send_settings_oid", data, room=room)


# ------------------------------------

@app.route('/profile')
def profile():
    if 'user' in session and 'login' in session:
        if session['login']:
            return render_template("profile.html",
                                   active_page='profile',
                                   role=session['role'],
                                   name=session['name'],
                                   )
    else:
        return redirect(url_for('login'))


@socketio.on("get_profile")
def get_profile():
    if 'user' in session:
        o_id = session['o_id']
        room = request.sid
        data = database_operations.get_profile(session['user'])
        if data == []:
            pass
        else:
            socketio.emit("send_profile", data, room=room)


@socketio.on("Profile_Update")
def profile_update(dataset):
    if 'user' in session:
        o_id = session['o_id']
        room = request.sid
        data = database_operations.update_profile(dataset, session['user'])
        socketio.emit("master_console", "Data saved successfully !", room=room)


# ----------------- Non Smartphone Users ----------------------------

@socketio.on("Sign_Up_Request_Non_Smartphone")
def sign_up_request_manual(data):
    room = session['user']
    join_room(room)
    data["o_id"] = session["o_id"]
    print(session)
    print(data)
    Val = database_operations.Sign_Up_Request_Manual(data)
    if Val == True:
        socketio.emit("sign_up_status", "Non Smartphone User Signed Up.", room=room)
    else:
        socketio.emit("sign_up_status", "This name and number already exists in organisation.", room=room)


@app.route('/manual_new')
def manual_new():
    if 'user' in session and 'login' in session:
        if session['login']:
            return render_template("non_smartphone.html",
                                   active_page='manual',
                                   role=session['role'],
                                   name=session['name'],
                                   )
    else:
        return redirect(url_for('login'))


@app.route('/manual_all')
def manual_all():
    if 'user' in session and 'login' in session:
        if session['login']:
            return render_template("manual_all.html",
                                   active_page='manual',
                                   role=session['role'],
                                   name=session['name'],
                                   )
    else:
        return redirect(url_for('login'))


@socketio.on("manual_application_data")
def manual_application_data():
    if 'user' in session:
        room = session['user']
        join_room(room)
        data = database_operations.manual_application_data(session['o_id'])
        socketio.emit("application", data, room=room)


@app.route('/manual_report')
def manual_report():
    if 'user' in session and 'login' in session:
        if session['login']:
            return render_template("Anzen.htm",
                                   active_page='manual',
                                   role=session['role'],
                                   name=session['name'],
                                   )
    else:
        return redirect(url_for('login'))


# -----------------------------------------
@app.route('/manual_report_o_id/<o_id>')
def manual_report_o_id(o_id):
    return render_template("Anzen.htm", o_id=o_id)


@socketio.on("manual_report_data_with_o_id")
def manual_report_data_with_o_id(o_id):
    room = session['user']
    join_room(room)
    data = database_operations.manual_report_data(o_id)
    socketio.emit("send_manual_report_data", data, room=room)


# -----------------------------------------------------------
@socketio.on("manual_report_data")
def manual_report_data():
    if 'user' in session:
        room = session['user']
        join_room(room)
        data = database_operations.manual_report_data(session['o_id'])
        socketio.emit("send_manual_report_data", data, room=room)


# --------------------------------------------------------------------------------------
# Contact Trace 

@app.route('/contact_trace_log', methods=['POST', 'GET'])
def contact_trace_log():
    if request.method == 'POST':
        return "wrong_method"
    else:
        data = {}
        data['phone'] = request.args.get('user_phone')
        data['uid'] = request.args.get('uid')
        data['rssi'] = request.args.get('rssi')
        data['timestamp'] = request.args.get('timestamp')
        data['uuid'] = request.args.get('uuid')
        data["social_distance"] = request.args.get('social_distance')
        data["total_contact"] = request.args.get('total_contact')
        data["team"] = request.args.get('team')
        data['pause'] = request.args.get('pause')

        print(" ------------------------------ ")
        print(" ------------------------------ ")
        print(data)
        print(" ------------------------------ ")
        print(" ------------------------------ ")

        return "data recieved"


# ===================================================
'''
NEW NOTIFICATIONS CODE
'''


# ===================================================
@app.route('/new_notifications', methods=['POST', 'GET'])
def new_notifications():
    data = {}
    session = {}
    session['name'] = "Android App"
    if request.method == 'POST':
        print(request.json)
        data = request.json
        print("------ Notification Request Recieved -----")
        print(data)
        print("-------------------------------------------")
        return jsonify({"response": "Notification Response"})


@app.route('/message_compose')
def message_compose():
    if 'user' in session and 'login' in session:
        if session['login']:
            return render_template("message_compose.html",
                                   active_page='notifications',
                                   role=session['role'],
                                   name=session['name'],
                                   )
    else:
        return redirect(url_for('login'))


@socketio.on("message_compose_data")
def message_compose_data():
    if 'user' in session:
        room = session['user']
        join_room(room)
        data = database_operations.message_compose_data(session['o_id'])
        socketio.emit("message_application", data, room=room)


@socketio.on("composed_message")
def composed_message(data):
    if 'user' in session:
        room = session['user']
        join_room(room)
        # data = database_operations.composed_message(session['o_id'])
        print("-------------------COMPOSED MESSAGE -------------")
        print(data)
        print("----------------------------------------")

        data = database_operations.send_message(data, session)

        # socketio.emit("message_application", data, room=room)


# -------------------------- Register USer TOKEN from Device ------------------------
@app.route('/register_token', methods=['POST', 'GET'])
def register_token():
    data = {}
    session = {}
    session['name'] = "App"
    if request.method == 'POST':
        print(request.json)
        data = request.json
        print("------ Register TOKEN Request Recieved -----")
        print(data)
        val = database_operations.add_user_registration_token(data)
        print("-------------------------------------------")
        return jsonify({"response": "RECEIVED"})


# ----------------------
# Question Settings
# ----------------------


@app.route('/questions')
def oid_questions():
    if 'user' in session:
        if session['user']:
            return render_template("question_settings.html",
                                   active_page='questions',
                                   role=session['role'],
                                   name=session['name'],
                                   )
    else:
        return redirect(url_for('login'))


@socketio.on("get_oid_questions")
def get_oid_questions():
    if 'user' in session:
        room = request.sid
        join_room(room)
        data = database_operations.get_questions_oid(session['o_id'])
        # print("--------------------------")
        # print(data)
        # print("--------------------------")
        to_send = data[0]["question_settings"]
        print("--------------------------")
        print(to_send)
        print("--------------------------")

        socketio.emit("sent_oid_questions", to_send, room=room)


@socketio.on("save_question_changes_oid")
def save_question_changes_oid(data):
    if 'user' in session:
        room = request.sid
        data, val = database_operations.save_questions_oid(data, session["o_id"])
        socketio.emit("master_console", "Saved !", room=room)
    else:
        print("User not found !")


# ------- MESSAGE HISTORY ----------

@app.route('/notifications')
def notifications():
    if 'user' in session and 'login' in session:
        if session['login']:
            return render_template("all_notifications.html",
                                   active_page='notifications',
                                   role=session['role'],
                                   name=session['name'],
                                   )
    else:
        return redirect(url_for('login'))


@socketio.on("get_all_messages")
def get_all_messages():
    if 'user' in session:
        room = request.sid
        val, data = database_operations.get_all_messages(session)
        print(" ------------------------ Get All Messages -----------------")
        if val == 0:
            socketio.emit("master_console", "No messages found !", room=room)
        if val == 1:
            socketio.emit("send_all_messages", data, room=room)


@socketio.on("attendance_data_csv")
def attendance_data_csv(data):
    if 'user' in session:
        room = request.sid
        # print(data)
        import numpy as np
        m, n = np.shape(data)
        # field names
        # print(m,n)
        print("Processing Data")
        for i in range(0, m):
            if str(data[i][10]).__contains__("UNFREEZE"):
                data[i][9] = "FROZEN"
            else:
                data[i][9] = " - "
            data[i][10] = ""
        print("Process complete")

        fields = ['Name', 'Number', 'Date', 'Timestamp', 'Employee Type', 'Employee ID', 'E Pass Status', 'Temperature',
                  'Attendance Status', 'Account Status']

        filename = "Documents/" + str(uuid.uuid1()) + ".csv"
        # writing to csv file
        with open(filename, 'w') as csvfile:
            # creating a csv writer object
            csvwriter = csv.writer(csvfile)

            # writing the fields
            csvwriter.writerow(fields)
            # writing the data rows
            csvwriter.writerows(data)

        socketio.emit("filename", filename, room=room)


@socketio.on("application_data_csv")
def application_data_csv():
    if 'user' in session:
        room = request.sid
        data = database_operations.application_data_csv(session["o_id"])
        # print(data[0])

        fields = ['Name', 'Number', 'Date', "Timestamp", 'Employee Type', 'Employee ID', 'Contract Organisation',
                  'Application Status']

        filename = "Documents/" + str(uuid.uuid1()) + ".csv"
        # writing to csv file
        with open(filename, 'w') as csvfile:
            # creating a csv writer object
            csvwriter = csv.writer(csvfile)

            # writing the fields
            csvwriter.writerow(fields)
            # writing the data rows
            csvwriter.writerows(data)

        socketio.emit("filename", filename, room=room)


@app.route('/Documents/<path:filename>')
def download_document(filename):
    print('correct loop')
    return send_from_directory('Documents', filename, as_attachment=True)


@socketio.on("freeze_account")
def freeze_account(phone):
    if 'user' in session:
        room = request.sid
        data = database_operations.freeze_account(phone)
        print(" ------------------------ Freeze Account -----------------")


@socketio.on("unfreeze_account")
def unfreeze_account(phone):
    if 'user' in session:
        room = request.sid
        data = database_operations.unfreeze_account(phone)
        print(" ------------------------ Freeze Account -----------------")


'''====================================================================='''
'''====================================================================='''
'''===================        OEE       ================================'''
'''====================================================================='''
'''====================================================================='''


@app.route('/file_upload', methods=['GET', 'POST'])
def file_upload():
    print("file loading")
    if request.method == 'POST':
        print("file received")
        f = request.files['file']

        f.save((planning_sheet_save_path + f.filename))
        print("file=", f.filename)
        return jsonify({'file_upload_successful': f.filename})

    return render_template("production_plan.html",
                           active_page='production_plan',
                           role=session['role'],
                           name=session['name'],
                           file_status="Uploaded Successfully!",
                           filename=f.filename
                           )


@socketio.on("sheet_name_oee")
def sheet_name_oee(data):
    if 'user' in session:
        room = request.sid
        join_room(room)
        print("============================")
        print(data["sheet_name"])
        print(data["file_name"])
        # data,error_quantity,error_partnumber,error_machine = read_planning_sheet.parse_excel("POS.xlsx", "20" )
        status, data, errors, errors_part_number, errors_machine, used_orders = read_planning_sheet.read_excel(
            planning_sheet_save_path + data["file_name"], data["sheet_name"])
        socketio.emit("uploaded_sheet_status", status, room=room)

        time.sleep(1)
        to_send = [
            str(len(data)),
            str(len(errors)),
            str(len(errors_part_number)),
            str(len(errors_machine)),
            str(len(used_orders)),
        ]
        print(to_send)
        socketio.emit("update_bar_graph", to_send, room=room)

        to_send = [
            str(len(data + errors + errors_part_number + errors_machine)),
            str(len(errors + errors_part_number + errors_machine)),
            str(len(data)),

        ]
        print(to_send)
        socketio.emit("update_round_graph", to_send, room=room)

        # print(data[0])
        new_data = []
        # STRF DATETIME
        for record in used_orders:
            try:
                record["DATE"] = record["DATE"].strftime("%d-%b-%Y")
                record["TIME"] = record["TIME"].strftime("%H:%M:%S")
                new_data.append(record)
            except:
                pass

        if len(new_data) != 0:
            socketio.emit("uploaded_used_data_oee", new_data, room=room)
            time.sleep(1)

        # print(data[0])
        new_data = []

        print(" ================== VALID ORDERS ====================")
        print(len(data))
        print(" =====================================================")

        # STRF DATETIME
        print()
        for record in data:
            try:
                record["DATE"] = record["DATE"].strftime("%d-%b-%Y")
                record["TIME"] = record["TIME"].strftime("%H:%M:%S")
                new_data.append(record)
            except:
                new_data.append(record)
                '''print(" ================== NEW VALID ORDERS ====================")
                print(json.dumps(record, indent=4))
                print(" =====================================================")'''

        if len(new_data) != 0:
            socketio.emit("uploaded_data_oee", new_data, room=room)
            time.sleep(1)

        new_data = []
        # STRF DATETIME
        for record in errors:
            try:
                record["DATE"] = record["DATE"].strftime("%d-%b-%Y")
                record["TIME"] = record["TIME"].strftime("%H:%M:%S")
                new_data.append(record)
            except:
                pass
        if len(new_data) != 0:
            socketio.emit("error_quantity_oee", new_data, room=room)
            time.sleep(1)

        time.sleep(1)
        new_data = []
        # STRF DATETIME
        for record in errors_part_number:
            try:
                record["DATE"] = record["DATE"].strftime("%d-%b-%Y")
                record["TIME"] = record["TIME"].strftime("%H:%M:%S")
                new_data.append(record)
            except:
                pass
        if len(new_data) != 0:
            socketio.emit("error_partnumber_oee", new_data, room=room)
            time.sleep(1)

        new_data = []
        # STRF DATETIME
        for record in errors_machine:
            try:
                record["DATE"] = record["DATE"].strftime("%d-%b-%Y")
                record["TIME"] = record["TIME"].strftime("%H:%M:%S")
                new_data.append(record)
            except:
                pass
        if len(new_data) != 0:
            socketio.emit("error_machine_oee", new_data, room=room)
            time.sleep(1)


@socketio.on("proceed_to_production_plan_2")
def proceed_to_production_plan_2(data):
    if 'user' in session:
        room = request.sid
        join_room(room)
        print("===============IN TEMP PRODUCTION PLAN=============")
        print(len(data))
        new_database_operations.temp_insert_production_orders(data, session)

        print(" PRODCUTION ORDERS INSERTED in TEMP DATA")


@socketio.on("proceed_to_production_plan_3")
def proceed_to_production_plan_3(data):
    if 'user' in session:
        room = request.sid
        join_room(room)
        print("===============IN TEMP PRODUCTION PLAN 2 =============")
        print(len(data))
        new_database_operations.temp_insert_production_orders(data, session)

        print(" PRODCUTION ORDERS INSERTED in TEMP DATA")


@socketio.on("save_production_plan")
def save_production_plan(data):
    if 'user' in session:
        room = request.sid
        join_room(room)
        print("============== SAVING PRODUCTION PLAN =============")
        print(len(data))
        # convert time string of datetime to time format
        # ADD stop time planned
        for record in data:
            record["PLANNED_START"] = datetime.datetime.strptime(record["DATE"] + " :: " + record["TIME"],
                                                                 '%d-%b-%Y :: %H:%M:%S')
            record["PLANNED_STOP"] = record["PLANNED_START"] + datetime.timedelta(
                seconds=(record["CYCLE TIME"] * record["NEW QTY"]))
            record["ACTUAL_START"] = "-"
            record["ACTUAL_STOP"] = "-"
            record["STATUS"] = "Planned"
            print(record["PLANNED_START"])

        new_database_operations.insert_production_orders(data)
        new_database_operations.clear_temp_production_orders(session)

        print(" PRODCUTION ORDERS INSERTED in TEMP DATA")


@socketio.on("get_temp_production_orders")
def get_production_orders():
    if 'user' in session:
        room = request.sid
        join_room(room)
        print("============SEND PRODUCTION ORDERS TEMP================")
        production_orders = new_database_operations.temp_get_production_orders(session)
        for production_order in production_orders:
            backend_params = new_database_operations.query_backend_database(production_order["PART NUMBER"])

            if bool(backend_params):

                if "DA_TAPE" in backend_params:
                    production_order["DA_TAPE"] = backend_params["DA_TAPE"]
                else:
                    production_order["DA_TAPE"] = "Not Found"

                if "TAB_TAPE" in backend_params:
                    production_order["TAB_TAPE"] = backend_params["TAB_TAPE"]
                else:
                    production_order["TAB_TAPE"] = "Not Found"

                if "child_parts" in backend_params:
                    production_order["child_parts"] = backend_params["child_parts"]
                else:
                    production_order["child_parts"] = ["Not Found"]

                # check if it is Primary or Secondary Machine
                print(json.dumps(backend_params, indent=4))
                if "PRIMARY MACHINE" in backend_params:
                    print("=======================================")
                    print(production_order["MACHINE"])
                    print(backend_params["PRIMARY MACHINE"])
                    print(backend_params["SECONDARY MACHINE"])

                    if production_order["MACHINE"] == backend_params["PRIMARY MACHINE"]:
                        print("Matched PM ")
                        production_order["MACHINE TYPE"] = "Primary"
                        production_order["CYCLE TIME"] = backend_params["PM CYCLE TIME(SEC)"]
                        if production_order["CYCLE TIME"] != "":
                            production_order["MACHINE STATUS"] = "Valid"
                        else:
                            production_order["MACHINE STATUS"] = "Invalid"
                        production_order["PRD/HR"] = backend_params["PM PRD/HR"]
                        production_order["PRD/SHIFT"] = backend_params["PM PRD/SHIFT"]
                        production_order["LINE REJECTION"] = backend_params["PM LINE REJECTION"]
                        production_order["START UP TIME (MIN)"] = backend_params["PM START UP TIME (MIN)"]
                        production_order["START UP SCRAP(KG)"] = backend_params["PM START UP SCRAP(KG) "]
                        production_order["MOULD CHANGE TIME (MIN)"] = backend_params["PM MOULD CHANGE TIME (MIN) "]

                    elif production_order["MACHINE"] == backend_params["SECONDARY MACHINE"]:
                        print("Matched SM ")
                        production_order["MACHINE TYPE"] = "Secondary"
                        production_order["CYCLE TIME"] = backend_params["SM CYCLE TIME(SEC)"]
                        if production_order["CYCLE TIME"] != "":
                            production_order["MACHINE STATUS"] = "Valid"
                        else:
                            production_order["MACHINE STATUS"] = "Invalid"
                        production_order["PRD/HR"] = backend_params["SM PRD/HR"]
                        production_order["PRD/SHIFT"] = backend_params["SM PRD/SHIFT"]
                        production_order["LINE REJECTION"] = backend_params["SM LINE REJECTION"]
                        production_order["START UP TIME (MIN)"] = backend_params["SM START UP TIME (MIN)"]
                        production_order["START UP SCRAP(KG)"] = backend_params["SM START UP SCRAP(KG) "]
                        production_order["MOULD CHANGE TIME (MIN)"] = backend_params["SM MOULD CHANGE TIME (MIN) "]

                    else:
                        print("No Match ")
                        production_order["MACHINE TYPE"] = " -- "
                        production_order["CYCLE TIME"] = ""
                        if production_order["CYCLE TIME"] != "":
                            production_order["MACHINE STATUS"] = "Valid"
                        else:
                            production_order["MACHINE STATUS"] = "Invalid"
                        production_order["PRD/HR"] = " -- "
                        production_order["PRD/SHIFT"] = " -- "
                        production_order["LINE REJECTION"] = " -- "
                        production_order["START UP TIME (MIN)"] = " -- "
                        production_order["START UP SCRAP(KG)"] = " -- "
                        production_order["MOULD CHANGE TIME (MIN)"] = " -- "

                    print("=======================================")

            else:
                production_order["DA_TAPE"] = "Not in Database"
                production_order["TAB_TAPE"] = "Not in Database"
                production_order["child_parts"] = ["Not in Database"]

                production_order["MACHINE TYPE"] = " -- "
                production_order["CYCLE TIME"] = ""
                if production_order["CYCLE TIME"] != "":
                    production_order["MACHINE STATUS"] = "Valid"
                else:
                    production_order["MACHINE STATUS"] = "Invalid"
                production_order["PRD/HR"] = " -- "
                production_order["PRD/SHIFT"] = " -- "
                production_order["LINE REJECTION"] = " -- "
                production_order["START UP TIME (MIN)"] = " -- "
                production_order["START UP SCRAP(KG)"] = " -- "
                production_order["MOULD CHANGE TIME (MIN)"] = " -- "

        socketio.emit("sent_temp_production_orders", production_orders, room=room)
        print(" TEMP PRODCUTION ORDERS SENT ")


@socketio.on("get_production_orders")
def get_production_orders():
    if 'user' in session:
        room = request.sid
        join_room(room)
        print("============================")
        production_orders = new_database_operations.get_production_orders()

        # Seriallize Planned Start Time and End Time

        for record in production_orders:
            try:
                current_time = datetime.datetime.now()
                if current_time < record["PLANNED_START"]:
                    record["STATUS"] = "Planned"
                if record["PLANNED_START"] <= current_time <= record["PLANNED_STOP"]:
                    record["STATUS"] = "Live"
                if current_time > record["PLANNED_STOP"]:
                    record["STATUS"] = "Done"
                if "ACTION" in record:
                    pass
                else:
                    record["ACTION"] = "Start"
                new_database_operations.update_specific_production_order(record)

                record["PLANNED_START"] = record["PLANNED_START"].strftime("%Y-%m-%d %H:%M:%S")
                record["PLANNED_STOP"] = record["PLANNED_STOP"].strftime("%Y-%m-%d %H:%M:%S")
                try:
                    record["ACTUAL_START"] = record["ACTUAL_START"].strftime("%Y-%m-%d %H:%M:%S")
                    record["ACTUAL_STOP"] = record["ACTUAL_STOP"].strftime("%Y-%m-%d %H:%M:%S")
                except:
                    pass


            except:
                pass
        socketio.emit("sent_production_orders", production_orders, room=room)
        print(" PRODCUTION ORDERS SENT ")


@socketio.on("update_specific_production_order")
def update_specific_production_order(data):
    if 'user' in session:
        room = request.sid
        join_room(room)
        print("============================")
        new_database_operations.update_specific_production_order(data)
        # socketio.emit("sent_production_orders", production_orders , room=room)
        # Seriallize Planned Start Time and End Time
        print("UPDATED PRODUCTION ORDERS")


@socketio.on("get_specific_production_order")
def get_specific_production_order(data):
    if 'user' in session:
        room = request.sid
        join_room(room)
        print("============================")
        record = new_database_operations.get_specific_production_order(data)
        record["PLANNED_START"] = record["PLANNED_START"].strftime("%Y-%m-%d %H:%M:%S")
        record["PLANNED_STOP"] = record["PLANNED_STOP"].strftime("%Y-%m-%d %H:%M:%S")
        try:
            record["ACTUAL_START"] = record["ACTUAL_START"].strftime("%Y-%m-%d %H:%M:%S")
            record["ACTUAL_STOP"] = record["ACTUAL_STOP"].strftime("%Y-%m-%d %H:%M:%S")
        except:
            pass

        socketio.emit("sent_specific_production_order", record, room=room)
        # Seriallize Planned Start Time and End Time
        print("SENT SPECIFIC PRODUCTION ORDERS")


@socketio.on("start_production_order")
def start_production_order(data):
    if 'user' in session:
        room = request.sid
        join_room(room)
        print("============================")
        # Update Status to In Process
        # Update Actual Start Time
        # Add Production Order Numbers to current shift
        actual_data = copy.deepcopy(data)
        print(actual_data)

        data["ACTUAL_START"] = datetime.datetime.now() + datetime.timedelta(constants.timezone_offset)
        data["ACTION"] = "In Process"
        new_database_operations.update_specific_production_order(data)
        to_add = {
                "ORDER NUMBER (LEFT)" : actual_data["ORDER NUMBER (LEFT)"],
                "ORDER NUMBER (RIGHT)" : actual_data["ORDER NUMBER (RIGHT)"]
            }

        new_database_operations.update_shift_production_orders(to_add)

        print("STARTED PRODUCTION ORDER")
        socketio.emit("console", "Production order Started", room=room)


@socketio.on("stop_production_order")
def stop_production_order(data):
    if 'user' in session:
        room = request.sid
        join_room(room)
        print("============================")
        # Update Status to In Process
        # Update Actual Start Time
        # Add Production Order Numbers to current shift
        actual_data = copy.deepcopy(data)
        print(actual_data)

        data["ACTUAL_STOP"] = datetime.datetime.now() + datetime.timedelta(constants.timezone_offset)
        data["ACTION"] = "Done"
        new_database_operations.update_specific_production_order(data)

        # new_database_operations.update_shift_production_orders([actual_data["ORDER NUMBER (LEFT)"],actual_data["ORDER NUMBER (LEFT)"]])

        print("STOPPED PRODUCTION ORDER")
        socketio.emit("console", "Production order Stopped", room=room)


@socketio.on("get_production_order_details_by_machine")
def get_production_order_details_by_machine(data):
    if 'user' in session:
        room = request.sid
        join_room(room)
        print("============================")
        # QUERY FORMAT
        record, found = new_database_operations.get_production_order_details_by_machine(data)
        if found:
            print("FOUND IN_PROCESS PRODUCTION ORDER")
            socketio.emit("console", "Production Order In Process", room=room)
            time.sleep(0.5)
            record["PLANNED_START"] = record["PLANNED_START"].strftime("%Y-%m-%d %H:%M:%S")
            record["PLANNED_STOP"] = record["PLANNED_STOP"].strftime("%Y-%m-%d %H:%M:%S")
            try:
                record["ACTUAL_START"] = record["ACTUAL_START"].strftime("%Y-%m-%d %H:%M:%S")
                record["ACTUAL_STOP"] = record["ACTUAL_STOP"].strftime("%Y-%m-%d %H:%M:%S")
            except:
                pass
            socketio.emit("send_production_order", record, room=room)
        else:
            print("PRODUCTION ORDER NOT FOUND !")
            socketio.emit("console", "No Production Order", room=room)
            time.sleep(0.5)


@socketio.on("save_part_weight_and_cavities")
def save_part_weight_and_cavities(data):
    if 'user' in session:
        room = request.sid
        join_room(room)
        print("============================")
        print(data)
        new_database_operations.update_specific_production_order(data)
        print("Updated Data !")


@app.route('/production_plan')
def index():
    if 'user' in session and 'login' in session:
        if session['login']:
            return render_template("production_plan.html",
                                   active_page='production_plan',
                                   role=session['role'],
                                   name=session['name'],
                                   file_status="Please Select a File to continue.",
                                   filename=""
                                   )
    else:
        return redirect(url_for('login'))


@app.route('/upload_production_plan')
def upload_production_plan():
    if 'user' in session and 'login' in session:
        if session['login']:
            return render_template("upload_production_plan.html",
                                   active_page='production_plan',
                                   role=session['role'],
                                   name=session['name'],
                                   file_status="Please Select a File to continue.",
                                   filename=""
                                   )
    else:
        return redirect(url_for('login'))


@app.route('/upload_production_plan_2')
def upload_production_plan_2():
    if 'user' in session and 'login' in session:
        if session['login']:
            return render_template("upload_production_plan_2.html",
                                   active_page='production_plan',
                                   role=session['role'],
                                   name=session['name'],
                                   file_status="Please Select a File to continue.",
                                   filename=""
                                   )
    else:
        return redirect(url_for('login'))


@app.route('/upload_production_plan_3')
def upload_production_plan_3():
    if 'user' in session and 'login' in session:
        if session['login']:
            return render_template("upload_production_plan_3.html",
                                   active_page='production_plan',
                                   role=session['role'],
                                   name=session['name'],
                                   file_status="Please Select a File to continue.",
                                   filename=""
                                   )
    else:
        return redirect(url_for('login'))


# ===============================================================
@app.route('/shift_plan')
def shift_plan():
    if 'user' in session and 'login' in session:
        if session['login']:
            return render_template("shift_plan.html",
                                   active_page='shift_plan',
                                   role=session['role'],
                                   name=session['name'],
                                   file_status="Please Select a File to continue.",
                                   filename=""
                                   )
    else:
        return redirect(url_for('login'))


@socketio.on("get_shift_records")
def get_shift_records(data):
    if 'user' in session:
        room = request.sid
        join_room(room)
        print("============================")
        shift_records = new_database_operations.get_shift_records(data)

        # Seriallize Planned Start Time and End Time

        for record in shift_records:
            try:
                current_time = datetime.datetime.now()
                if current_time < record["SHIFT START TIME"]:
                    record["STATUS"] = "Planned"
                if record["SHIFT START TIME"] <= current_time <= record["SHIFT STOP TIME"]:
                    record["STATUS"] = "Live"
                if current_time > record["SHIFT STOP TIME"]:
                    record["STATUS"] = "Done"
                record["SHIFT START TIME"] = record["SHIFT START TIME"].strftime("%Y-%m-%d %H:%M:%S")
                record["SHIFT STOP TIME"] = record["SHIFT STOP TIME"].strftime("%Y-%m-%d %H:%M:%S")
            except:
                pass
        socketio.emit("sent_shift_records", shift_records, room=room)
        print("SHIFT RECORDS SENT ")


@socketio.on("get_shift_plan_dates")
def get_shift_plan_dates():
    if 'user' in session:
        room = request.sid
        join_room(room)
        print("============================")
        temp = datetime.datetime.now() + datetime.timedelta(minutes=constants.timezone_offset)
        to_send = {}

        to_send["todays_date"] = temp.strftime("%Y-%m-%d")
        to_send["tomorrows_date"] = (temp + datetime.timedelta(hours=24)).strftime("%Y-%m-%d")

        print(to_send)
        socketio.emit("send_shift_plan_dates", to_send, room=room)
        print("SHIFT DATES SENT ")


@socketio.on("save_shift_data")
def save_shift_data(data):
    if 'user' in session:
        room = request.sid
        join_room(room)
        print("============================")
        print(data)
        new_database_operations.update_shift_records(data)
        # print(to_send)

        print("SHIFT DATA UPDATED")


@socketio.on("get_specific_shift_record")
def get_specific_shift_record(data):
    if 'user' in session:
        room = request.sid
        join_room(room)
        print("============================")
        print(data)
        record = new_database_operations.get_specific_shift_record(data)
        print(record)
        try:
            current_time = datetime.datetime.now()
            if current_time < record["SHIFT START TIME"]:
                record["STATUS"] = "Planned"
            if record["SHIFT START TIME"] <= current_time <= record["SHIFT STOP TIME"]:
                record["STATUS"] = "Live"
            if current_time > record["SHIFT STOP TIME"]:
                record["STATUS"] = "Done"
            record["SHIFT START TIME"] = record["SHIFT START TIME"].strftime("%Y-%m-%d %H:%M:%S")
            record["SHIFT STOP TIME"] = record["SHIFT STOP TIME"].strftime("%Y-%m-%d %H:%M:%S")
        except:
            pass

        socketio.emit("send_specific_shift_record", record, room=room)
        print("SPECIFIC SHIFT DATA SENT ")




'''
=================================================================================================================
DOWNTIME
=================================================================================================================
'''
@app.route('/downtime_post', methods=['POST', 'GET'])
def downtime_post():
    if request.method == 'POST':
        data = request.json
        print(data)
        # check if production order is on that machine
        production_order = new_database_operations.get_production_order_from_machine(data)
        if production_order is not None :
            if len(data["data"])>0:
                for downtime_record in data["data"] :
                    downtime_record["ORDER NUMBER (LEFT)"] = production_order["ORDER NUMBER (LEFT)"]
                    downtime_record["ORDER NUMBER (RIGHT)"] = production_order["ORDER NUMBER (RIGHT)"]
                    to_send_back = new_database_operations.update_downtime(downtime_record)
                    print("SOFTWARE DOWNTIME RECORDED !")

        print("Data Recevied")
        return "Data Recieved"
    else:
        return "wrong_method"

@socketio.on("get_current_time_start")
def get_current_time_start():
    if 'user' in session:
        room = request.sid
        join_room(room)
        print("============================")
        temp = datetime.datetime.now() + datetime.timedelta(minutes=constants.timezone_offset)
        to_send = {}

        to_send["current_time"] = temp.strftime(constants.time_format)

        print(to_send)

        socketio.emit("send_current_time_start", to_send, room=room)
        print("CURRENT TIME SENT !")

@socketio.on("get_current_time_stop")
def get_current_time_stop():
    if 'user' in session:
        room = request.sid
        join_room(room)
        print("============================")
        temp = datetime.datetime.now() + datetime.timedelta(minutes=constants.timezone_offset)
        to_send = {}
        to_send["current_time"] = temp.strftime(constants.time_format)
        print(to_send)
        socketio.emit("send_current_time_stop", to_send, room=room)
        print("CURRENT TIME SENT !")

@socketio.on("get_downtime_record")
def get_downtime_record(data):
    if 'user' in session:
        room = request.sid
        join_room(room)
        socketio.emit("console_2", "Recording Downtime !", room=room)
        to_send_back = new_database_operations.update_downtime(data)
        socketio.emit("downtime_chart_update", to_send_back, room=room)
        time.sleep(2)
        socketio.emit("console_2", "Downtime Successfully Recorded !", room=room)
        print("DOWNTIME RECORDED !")

@socketio.on("update_downtime_charts")
def update_downtime_charts(data):
    if 'user' in session:
        room = request.sid
        join_room(room)
        to_send_back = new_database_operations.update_downtime_charts(data)
        socketio.emit("downtime_chart_update", to_send_back, room=room)
        time.sleep(2)
        socketio.emit("console_2", "Downtime Successfully Recorded !", room=room)
        print("DOWNTIME CHART UPDATED!")

@socketio.on("change_downtime_status")
def change_downtime_status(data):
    if 'user' in session:
        room = request.sid
        join_room(room)
        print(data)
        new_database_operations.update_downtime_status(data)
        print("Done Updating Downtime !")

@socketio.on("change_downtime_type")
def change_downtime_type(data):
    if 'user' in session:
        room = request.sid
        join_room(room)
        print(data)
        new_database_operations.update_downtime_type(data)
        print("Done Updating Downtime !")


@app.route('/downtime', methods=['GET', 'POST'])
def downtime():
    if 'user' in session and 'login' in session:
        if session['login']:
            return render_template("downtime_analysis.html",
                                   active_page='downtime',
                                   role=session['role'],
                                   name=session['name'],
                                   )
    else:
        return redirect(url_for('login'))


'''
=================================================================================================================
REJECTION
=================================================================================================================
'''

@socketio.on("rejection_details_records")
def rejection_details_records(data):
    if 'user' in session:
        room = request.sid
        join_room(room)
        socketio.emit("rejection_console_update", "", room=room)
        to_send_back = new_database_operations.update_rejection(data)
        socketio.emit("rejection_chart_update", to_send_back, room=room)
        time.sleep(2)
        socketio.emit("rejection_console_update", "Rejection Successfully Recorded !", room=room)
        print("REJECTION CHART UPDATED!")

@socketio.on("startup_rejection_details_records")
def startup_rejection_details_records(data):
    if 'user' in session:
        room = request.sid
        join_room(room)
        socketio.emit("startup_rejection_details_console", "", room=room)
        to_send_back = new_database_operations.update_startup_rejection(data)
        socketio.emit("startup_rejection_values_update", to_send_back, room=room)
        time.sleep(2)
        socketio.emit("startup_rejection_details_console", "Rejection Successfully Recorded !", room=room)
        print("START UP REJECTION UPDATED!")

@socketio.on("record_process_scrap")
def record_process_scrap(data):
    if 'user' in session:
        room = request.sid
        join_room(room)
        socketio.emit("record_process_scrap_console", "", room=room)
        new_database_operations.update_process_scrap(data)
        time.sleep(1)
        socketio.emit("record_process_scrap_console", "Process Scrap Updated Successfully !", room=room)

        print("PROCESS SCRAP UPDATED!")

@socketio.on("update_rejection_chart")
def update_rejection_chart(data):
    if 'user' in session:
        room = request.sid
        join_room(room)
        to_send_back = new_database_operations.update_rejection_chart(data)
        time.sleep(1)
        socketio.emit("rejection_chart_update", to_send_back , room=room)
        print("REJECTION CHART UPDATED!")

@socketio.on("update_pie_chart")
def update_pie_chart(data):
    if 'user' in session:
        room = request.sid
        join_room(room)
        to_send_back = new_database_operations.update_rejection_piechart (data)
        time.sleep(1)
        socketio.emit("rejection_piechart_update", to_send_back , room=room)
        print("REJECTION PIE CHART UPDATED!")

@app.route('/rejection', methods=['GET', 'POST'])
def rejection():
    if 'user' in session and 'login' in session:
        if session['login']:
            return render_template("rejection_analysis.html",
                                   active_page='rejection',
                                   role=session['role'],
                                   name=session['name'],
                                   )
    else:
        return redirect(url_for('login'))

@socketio.on("change_rejection_status")
def change_rejection_status(data):
    if 'user' in session:
        room = request.sid
        join_room(room)
        print(data)
        new_database_operations.update_rejection_status(data)
        print("Done Updating Rejection !")

'''
=================================================================================================================
BASIC DETAILS
=================================================================================================================
'''

@app.route('/shot_data_post', methods=['POST', 'GET'])
def shot_post():
    if request.method == 'POST':
        data = request.json
        print(data)
        # UPDATE COUNTER
        new_data = {}
        new_data['machine'] = data['machine']
        new_data['new_shots'] = data['total_shots']
        new_data['timestamp'] = data['timestamp']

        # Check if there is a production order on that machine
        new_database_operations.update_shot_data(new_data)


        production_order = new_database_operations.get_production_order_from_machine(data)
        if production_order is not None :
            if len(data["shot_details"])>0:
                for shot_record in data["shot_details"] :
                    shot_record["ORDER NUMBER (LEFT)"] = production_order["ORDER NUMBER (LEFT)"]
                    shot_record["ORDER NUMBER (RIGHT)"] = production_order["ORDER NUMBER (RIGHT)"]
                    to_send_back = new_database_operations.update_shot_details(shot_record)
                    print("SOFTWARE SHOT DATA RECORDED !")

        print("Data Recevied")
        return "Data Recieved"
    else:
        return "wrong_method"

@app.route('/shot_data', methods=['POST', 'GET'])
def add_shot_data():
    if request.method == 'POST':
        return "wrong_method"
    else:
        data = {}
        data['machine'] = request.args.get('machine')
        data['new_shots'] = request.args.get('new_shots')
        data['timestamp'] = request.args.get('timestamp')

        # Check if there is a production order on that machine
        new_database_operations.update_shot_data(data)

        print(" ---------- SHOT DATA RECEIVED -------------------- ")
        print(data)
        print(" ------------------------------ ")

        return "data recieved"

@app.route('/basic', methods=['GET', 'POST'])
def basic():
    if 'user' in session and 'login' in session:
        if session['login']:
            return render_template("basic_details_analysis.html",
                                   active_page='basic',
                                   role=session['role'],
                                   name=session['name'],
                                   )
    else:
        return redirect(url_for('login'))

'''
=================================================================================================================
GENERIC DETAILS
=================================================================================================================
'''


@app.route('/plant_details')
def plant_details():
    if 'user' in session and 'login' in session:
        if session['login']:
            return render_template("ppap_dash.html",
                                   active_page='plant_details',
                                   role=session['role'],
                                   name=session['name'],
                                   )
    else:
        return redirect(url_for('login'))

'''
=================================================================================================================
SHIFT PRODUCTION REPORT
=================================================================================================================
'''
@app.route('/spr')
def spr():
    if 'user' in session and 'login' in session:
        if session['login']:
            return render_template("SPR1.html",
                                   active_page='spr',
                                   role=session['role'],
                                   name=session['name'],
                                   )
    else:
        return redirect(url_for('login'))

@app.route('/SPR_template/<shift_date>/<shift_id>/<machine>')
def SPR_template(shift_date,shift_id,machine):
    if 'user' in session and 'login' in session:
        if session['login']:
            return render_template("SPR_template.html",
                                   active_page='spr',
                                   role=session['role'],
                                   name=session['name'],
                                   shift_date = shift_date,
                                   shift_id = shift_id,
                                   machine = machine
                                   )
    else:
        return redirect(url_for('login'))

@socketio.on("get_SPR_data")
def get_SPR_data(data):
    if 'user' in session:
        room = request.sid
        join_room(room)
        print(data)
        if "SHIFT DATE" in data:
            data["SHIFT DATE"] = data["SHIFT DATE"].replace("-","/")
            print(data)
            #new_database_operations.update_rejection_status(data)
            SPR_DATA = new_database_operations.get_shift_report(data)
            socketio.emit("send_SPR_data",SPR_DATA,room=room)
        print("Done Sending Shift Details !")




@app.before_first_request
def activate_job():
    def run_job():
        while True:
            print("RUNNING CRON JOB")
            current_time = datetime.datetime.now()
            new_database_operations.cron_job(current_time)
            time.sleep(constants.cron_job_time)

    thread = threading.Thread(target=run_job)
    thread.start()

'''@socketio.on("get_current_shift_SPR_page")
def get_current_shift_SPR_page():
    if 'user' in session:
        room = request.sid
        join_room(room)
        print("------------------------------------------------------GET CURRENT SHIFT SPR "
              "PAGE-------------------------------------------")
        shift = new_database_operations.get_current_shift_details()
        to_send = {
            "SHIFT DATE" : shift["SHIFT DATE"],
            "SHIFT ID" : shift["SHIFT ID"]
        }
        socketio.emit("shift_details_SPR_page",to_send,room=room)'''




'''
=================================================================================================================
TO DO
=================================================================================================================
'''

@app.route('/hierarchy')
def hierarchy():
    if 'user' in session and 'login' in session:
        if session['login']:
            return render_template("hirarchy.html",
                                   active_page='hierarchy',
                                   role=session['role'],
                                   name=session['name'],
                                   )
    else:
        return redirect(url_for('login'))

@socketio.on("active_machines")
def active_machine_data():
    active_machine_list=[]
    active_machine_data = new_database_operations.get_all_active_production_orders()
    for i in active_machine_data:
        active_machine_list.append(i['MACHINE'])
    active_machine_list = list(set(active_machine_list))
    print(active_machine_list)
    socketio.emit("received_active_machines",active_machine_list)


'''
@socketio.on("get_SPR_data")
def get_SPR_data(data):
    if 'user' in session:
        room = request.sid
        join_room(room)
        print(data)
        if "SHIFT DATE" in data:
            data["SHIFT DATE"] = data["SHIFT DATE"].replace("-","/")
            print(data)
            #new_database_operations.update_rejection_status(data)
            SPR_DATA = new_database_operations.get_shift_report(data)
            socketio.emit("send_SPR_data",SPR_DATA,room=room)
        print("Done Sending Shift Details !")
'''

@app.route('/pqcr')
def pqcr():
    if 'user' in session and 'login' in session:
        if session['login']:
            return render_template("PQCR1.html",
                                   active_page='pqcr',
                                   role=session['role'],
                                   name=session['name'],
                                   )
    else:
        return redirect(url_for('login'))

@app.route('/pqcr_template/<shift_date>/<machine>')
def pqcr_template(shift_date,machine):
    if 'user' in session and 'login' in session:
        if session['login']:
            return render_template("pqcr_template.html",
                                   active_page='spr',
                                   role=session['role'],
                                   name=session['name'],
                                   shift_date = shift_date,
                                   #shift_id = shift_id,
                                   machine = machine
                                   )
    else:
        return redirect(url_for('login'))


'''@socketio.on("get_SPR_data")
def get_SPR_data(data):
    if 'user' in session:
        room = request.sid
        join_room(room)
        print(data)
        if "SHIFT DATE" in data:
            data["SHIFT DATE"] = data["SHIFT DATE"].replace("-","/")
            print(data)
            #new_database_operations.update_rejection_status(data)
            SPR_DATA = new_database_operations.get_shift_report(data)
            socketio.emit("send_SPR_data",SPR_DATA,room=room)
        print("Done Sending Shift Details !")'''

@socketio.on("flaash")
def flaash(data):
    if 'user' in session:
        room = request.sid
        join_room(room)
        print(data)
    
        fname = os.path.join(app.static_folder,'data.json')
        with open(fname) as json_file:
            data1 = json.load(json_file)
        socketio.emit("show_json",data1)



@app.route('/quality')
def quality():
    return redirect(url_for('plant_details'))


@app.route('/all_apps')
def all_apps():
    if 'user' in session and 'login' in session:
        if session['login']:
            return render_template("all_apps.html",
                                   active_page='all_apps',
                                   role=session['role'],
                                   name=session['name'],
                                   )
    else:
        return redirect(url_for('login'))


# ===================================================================
@app.route('/overview')
def cbm_overview():
    if 'user' in session and 'login' in session:
        if session['login']:
            return render_template("overview.html",
                                   active_page='overview',
                                   role=session['role'],
                                   name=session['name'],
                                   )
    else:
        return redirect(url_for('login'))


@app.route('/events')
def events():
    if 'user' in session and 'login' in session:
        if session['login']:
            return render_template("events.html",
                                   active_page='events',
                                   role=session['role'],
                                   name=session['name'],
                                   )
    else:
        return redirect(url_for('login'))


@app.route('/assets')
def assets():
    if 'user' in session and 'login' in session:
        if session['login']:
            return render_template("assets.html",
                                   active_page='assets',
                                   role=session['role'],
                                   name=session['name'],
                                   )
    else:
        return redirect(url_for('login'))


@app.route('/kpi')
def kpi():
    if 'user' in session and 'login' in session:
        if session['login']:
            return render_template("kpi.html",
                                   active_page='kpi',
                                   role=session['role'],
                                   name=session['name'],
                                   )
    else:
        return redirect(url_for('login'))


@app.route('/rules')
def rules():
    return render_template("rules.html")


@app.route('/predictive')
def predictive():
    return render_template("predictive.html")


@app.route('/graphs')
def graphs():
    if 'user' in session and 'login' in session:
        if session['login']:
            return render_template("graphs.html",
                                   active_page='graphs',
                                   role=session['role'],
                                   name=session['name'],
                                   )
    else:
        return redirect(url_for('login'))


@app.route('/dashboards')
def dashboard():
    return render_template("dashboard_new.html")


# ========================================= fleet ==========================================

get_trip = ""
import retrive_trip_data


# ======================all route rendering htmls pages============================

@app.route('/fleet')
def fleet():
    if 'user' in session and 'login' in session:
        if session['login']:
            return render_template("trips.html",
                                   active_page='fleet',
                                   role=session['role'],
                                   name=session['name'],
                                   )
    else:
        return redirect(url_for('login'))


@app.route('/trip_details')
def trip_detail():
    if 'user' in session and 'login' in session:
        if session['login']:
            return render_template("trip_detail.html",
                                   active_page='trip_details',
                                   role=session['role'],
                                   name=session['name'],
                                   )
    else:
        return redirect(url_for('login'))


@app.route('/trip_schedule')
def trip_schedule():
    return render_template("schedule.html")


# ==================Start socket =======================================
# req for all trips
@socketio.on("req_all_trips")
def req_all_trips():
    print("yess worked")
    trips = retrive_trip_data.get_all_trips()
    socketio.emit("response_all_trips", trips)


@socketio.on("get_trip_data")
def get_trip_data(dt):
    global get_trip
    dt = json.loads(dt)
    get_trip = retrive_trip_data.get_trip_data(dt)
    # socketio.emit("response_all_trips",trips)


# -----------------------view more actions sockets here--------------------------------------

@socketio.on("req_trip_data")
def req_trip_data():
    global get_trip
    socketio.emit("response_trip_data", get_trip)


# ================================= Traceability  ==========================
import db_operation


@app.route('/bin_locations')
def bin_locations():
    return render_template("bin_locations.html")


@app.route('/empty_trolly')
def empty_trolly():
    return render_template("empty_trolly_storage.html")


@app.route('/empty_bin')
def empty_bin():
    if 'user' in session and 'login' in session:
        if session['login']:
            return render_template("empty_bin_storage.html",
                                   active_page='empty_bin',
                                   role=session['role'],
                                   name=session['name'],
                                   )
    else:
        return redirect(url_for('login'))


@app.route('/production_area')
def production_area():
    return render_template("production_area.html")


@app.route('/fg_storage')
def fg_storage():
    return render_template("fg_storage.html")


@app.route('/dispatch_preparation')
def dispatch_preparation():
    return render_template("dispatch_preparation.html")


@app.route('/in_transit')
def in_transit():
    return render_template("in_transit.html")


@app.route('/traceability')
def history():
    if 'user' in session and 'login' in session:
        if session['login']:
            return render_template("history.html",
                                   active_page='trace',
                                   role=session['role'],
                                   name=session['name'],
                                   )
    else:
        return redirect(url_for('login'))


# ==========================================================================================

@socketio.on("request_percentage")
def request_percentage():
    db_operation.bin_percentage(socketio)


@socketio.on("table_data")
def table_data(area):
    data = db_operation.table_data(area)
    socketio.emit("returned_table_data", data)


@socketio.on("history_bin")
def history_bin(data):
    response = db_operation.history_bin(data)
    socketio.emit("returned_table_data", response)


@socketio.on("history_gate")
def history_gate(data):
    response = db_operation.history_gate(data)
    socketio.emit("returned_table_data", response)


@socketio.on("all_data")
def all_data(x):
    print(">>>>>>>>>>>>>>>>>", x)
    response = db_operation.all_data_hist(x)
    socketio.emit("returned_table_data", response)


@app.route('/data_receiver', methods=['GET', 'POST'])
def data_receiver():
    if request.method == 'POST':
        sessions_data = request.get_json(force=True)
        print("======================shot Get", sessions_data)
        db_operation.bin_updator(sessions_data)
        socketio.emit("gate_blinker", sessions_data["bins"]["movement"]["gate"])
    return "ok"







@app.route('/uploader', methods=['GET', 'POST'])
def upload_file():
    if request.method == 'POST':
        f = request.files['file']
        f.save("f.xlsx")
        return 'file uploaded successfully'





@app.route('/SPR/<machine>/<shift_date>/<shift_number>')
def report_of_a_type(machine,shift_date, shift_number):
    print("SPR File Name : ", (machine +"/" + shift_date + "_" + shift_number + ".pdf"))
    return send_from_directory(SPR_path, (machine + "/" + shift_date + "_" + shift_number + ".pdf"))

    '''if os.path.exists(SPR_path+ "/" + machine + "/" +shift_date + "_" + shift_number + ".pdf"):
        print("Path Not Found.")
        return send_from_directory(SPR_path, (machine + "/" + shift_date + "_" + shift_number + ".pdf"))
    else :
        return send_from_directory(SPR_path, "Not_Found.pdf")'''


@app.route('/RFID_DATA', methods=['GET', 'POST'])
def rfid_test():
    if request.method == 'POST':
        sessions_data = request.get_json(force=True)
        print("======================Received Data ============", sessions_data)

        current_time = datetime.datetime.now().strftime(constants.spr_time_format)
        tags_save_path = "../RFID SCANS/"
        filename = tags_save_path + current_time + ".json"
        with open(filename, 'w') as fp:
            json.dump(sessions_data, fp)
        #db_operation.bin_updator(sessions_data)
        #socketio.emit("gate_blinker", sessions_data["bins"]["movement"]["gate"])
    return "RFID TAGS DATA RECEIVED"






'''@app.route('/REPORT/<jobnumber>')
def report(jobnumber):
    print("Report Requested")
    print(jobnumber)
    print("----------------")

    job_details = database.query_jobdetails({"jobnumber": str(jobnumber)})
    job_details = job_details[0]
    print(job_details)
    print(job_details["jobtype"])

    # job_data = database.get_job_data(jobnumber=str(jobnumber))
    # print(job_data)

    return render_template('REPORTS/iframe_trial.html',
                           src="/Report/" + job_details["jobtype"] + "/" + str(jobnumber))'''

if __name__ == "__main__":
    socketio.run(app,debug=True)
